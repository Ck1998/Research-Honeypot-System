This folder should contain the IP to Geolocation Database provided by MaxMindDB. 
[link](https://dev.maxmind.com/geoip/geoip2/geolite2/)
Since the file is greater than 25Mb, it can not be uploaded. The sample database is available for free use.
